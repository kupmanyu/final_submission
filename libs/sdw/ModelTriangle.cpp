#include "ModelTriangle.h"
#include <utility>

ModelTriangle::ModelTriangle() = default;

ModelTriangle::ModelTriangle(const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, Colour trigColour) :
		name(), vertices({{v0, v1, v2}}), vnormals({}), texturePoints(), bumpPoints(), colour(std::move(trigColour)), normal(), gloss(8.0f), reflection(false), ior_air(1.0f),
		ior_mat(0.0f) {}
ModelTriangle::ModelTriangle(const std::string tname, const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, Colour trigColour) :
		name(std::move(tname)), vertices({{v0, v1, v2}}), vnormals({}), texturePoints(), bumpPoints(), colour(std::move(trigColour)), normal(), gloss(8.0f), reflection(false),
		ior_air(1.0f), ior_mat(0.0f) {}

std::ostream &operator<<(std::ostream &os, const ModelTriangle &triangle) {
	os << "(" << triangle.vertices[0].x << ", " << triangle.vertices[0].y << ", " << triangle.vertices[0].z << ")\n";
	os << "(" << triangle.vertices[1].x << ", " << triangle.vertices[1].y << ", " << triangle.vertices[1].z << ")\n";
	os << "(" << triangle.vertices[2].x << ", " << triangle.vertices[2].y << ", " << triangle.vertices[2].z << ")\n";
	return os;
}
