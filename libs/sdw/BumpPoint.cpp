#include "BumpPoint.h"

BumpPoint::BumpPoint() = default;
BumpPoint::BumpPoint(float xPos, float yPos) : x(xPos), y(yPos) {}

std::ostream &operator<<(std::ostream &os, const BumpPoint &point) {
	os << "x: " << point.x << " y: " << point.y;
	return os;
}
