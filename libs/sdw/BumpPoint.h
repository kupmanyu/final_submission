#pragma once

#include <iostream>

struct BumpPoint {
	float x{};
	float y{};

	BumpPoint();
	BumpPoint(float xPos, float yPos);
	friend std::ostream &operator<<(std::ostream &os, const BumpPoint &point);
};
