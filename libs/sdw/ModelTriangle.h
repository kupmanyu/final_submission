#pragma once

#include <glm/glm.hpp>
#include <string>
#include <array>
#include "Colour.h"
#include "TexturePoint.h"
#include "BumpPoint.h"

struct ModelTriangle {
	std::string name{};
	std::array<glm::vec3, 3> vertices{};
	std::array<glm::vec3, 3> vnormals{};
	std::array<TexturePoint, 3> texturePoints{};
	std::array<BumpPoint, 3> bumpPoints{};
	Colour colour{};
	glm::vec3 normal{};
	float gloss{};
	bool reflection{};
	float ior_air{};
	float ior_mat{};

	ModelTriangle();
	ModelTriangle(const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, Colour trigColour);
	ModelTriangle(const std::string tname, const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, Colour trigColour);
	friend std::ostream &operator<<(std::ostream &os, const ModelTriangle &triangle);
};
